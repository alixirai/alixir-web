import Prismic from '@prismicio/client'

const api = 'https://alixir-website.prismic.io/api/v2'

export const getPage = async (page, debug) => {
    const response = await  Prismic.client(api, {})
    const results = await response.getSingle(page)
    debug && console.log(results.data)
    return results.data
}