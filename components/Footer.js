import getConfig from 'next/config'

import Linkedin from "./svg-components/linkedin"
import Twitter from "./svg-components/twitter"
import Mail from "./svg-components/mail"

import MailchimpSubscribe from "react-mailchimp-subscribe"

const { publicRuntimeConfig } = getConfig()
const { MAILCHIMP_URL } = publicRuntimeConfig

const Footer = () =>{
    return(
        <div className="page-footer">
            <div className="page-footer-content">
                <div className="social">
                    <div className='footer-logo'>
                        <img src="/logo.png" alt="Alixir" />
                        <span>ALIXIR</span>
                        <div className="social-links">
                            <p>Stay Connected</p>
                            <div className="social-links-group">
                                <a href="https://www.linkedin.com/company/alixir/">
                                    <Linkedin noBG={true}/>
                                </a>
                                <a href="https://mobile.twitter.com/alixirhq">
                                    <Twitter/>
                                </a>
                                <a href="mailto:joe@alixir.ai">
                                    <Mail/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="link-container">
                    <p>About</p>
                    <div>
                        <a href="http://mammography.alixir.ai/" target="_blank">Alixir AIMS</a> {/*will link it to aims desc page*/}
                        <a href="#team">Team</a>
                        <a href="#">Contact Sales</a> {/*will connect this to sales page*/}
                    </div>
                </div>
                <div className="newsletter">
                    <p>Newsletter:</p>
                    <MailchimpSubscribe 
                        url={MAILCHIMP_URL}
                        fields={[
                            {
                              placeholder: 'Enter your email',
                              required: true
                            }
                          ]}
                    />
                </div>
            </div>
            <div className="page-footer-foot">
                <span>Copyright © {Date().split(' ')[3]} - Alixir</span>
                <span>All Rights Reserved.</span>
            </div>
        </div>
    )
}

export default Footer;