const { useEffect } = require("react")

const RequestModal = () =>{
	useEffect(()=>{
		const modal = document.querySelector("#request-demo-modal");
		const triggers = document.querySelectorAll(".demo-trigger");
		const close = document.querySelector("span.close");

		triggers.forEach((trig)=>{
			trig.onclick= () =>{
				modal.classList.toggle("active")
			}
		})

		close.onclick = () => {
			modal.classList.toggle("active")
		}
	},[])

	return(
		<div id="request-demo-modal" className="modal">
			<div className="modal-content">
				<span className="close">&times;</span>
				<div className="intro">
					<p className="brand-title"><span>AIMS</span> by Alixir</p>

					<p className="content">
						AIMS is in continuous development as we move towards regulatory approval.  If you are interested in learning more about the technology behind AIMS, and how it can provide a scalable and low-cost alternative or adjunct to a radiologist in breast cancer screening and detection, send us a message and we will arrange a suitable date and time.
						<strong></strong>
					</p>
				</div>
				<div className="form-container">
					<p className="heading">Request AIMS<sup>TM</sup> Demo</p>
					<form
						action="https://formspree.io/f/xnqlvvdp"
						method="POST"
						className="modal-form"
						id = "request-demo-form"
					>
						<input type="text" name="name" placeholder="Full Name*" required/>
						<input type="email" name="_replyto" placeholder="Your Email*" required/>
						<select id="role-select" name="role" form="request-demo-form" required>
							<option value selected="selected">You are a...*</option>
							<option value="Hospital/Clinic">Hospital / Clinic</option>
							<option value="Radiologist">Radiologist</option>
							<option value="PACS Company">PACS company</option>
							<option value="Healthcare IT">Healthcare IT</option>
							<option value="NGO">NGO</option>
							<option value="Other">Other</option>
						</select>

						<input type="text" name="org_link" placeholder="Link to your profile or organisation's website*" required/>

						<textarea name="message" placeholder="Your Message*" required></textarea>


						<button type="submit">Submit</button>
					</form>
				</div>
			</div>
		</div>
	)
}

export default RequestModal;