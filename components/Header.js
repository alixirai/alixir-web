import { useEffect, useState } from "react"
import Sun from "../components/svg-components/sun"
import Moon from "../components/svg-components/moon"

const Header = () => {

	const [mode,setMode] = useState(null)

	useEffect(()=>{
		const header = document.querySelector('.page-header')
		window.addEventListener('scroll',()=>{
			window.pageYOffset>0 ?
				header.classList.add('scrolled')
				:
				header.classList.remove('scrolled')
		})

		const initialState = localStorage.getItem('modeState') || 'day';
		setMode(initialState)
		document.querySelector('html').classList.add(initialState)
	})

	const modeChanger = () => {
		const newMode = mode=='day'?'night':'day'
		const micons = document.querySelectorAll('.mode-changer-icon')
		micons.forEach((k)=>k.classList.toggle('hide'))

		document.querySelector('html').classList = [newMode]
		localStorage.setItem('modeState', newMode);
		setMode(newMode);
	}

	const menuToggle = () => {
		const hamburger = document.querySelector('.menu_toggle')
		const body = document.querySelector('body')
		const menuBody = document.querySelector('.overlay-menu')

		body.classList.toggle('menu-active')
		menuBody.classList.toggle('is_active')
		hamburger.classList.toggle('is_active')
	}

	return(
		<div className="page-header">
			<button className="menu_toggle" onClick={menuToggle}>Menu</button>
			<div className="logo-container">
				<img src="/logo.png" className="logo" alt="Alixir logo" />
			</div>
			<ul className="navlist">
				<li className="navlist-item">
					<a href="#aboutus">About us</a>
				</li>
				<li className="navlist-item">
					<a href="#team">Team</a>
				</li>
				<li className="navlist-item">
					<a href="#partners">Partners</a>
				</li>
				<li className="navlist-item">
					<a href="http://mammography.alixir.ai/">AIMS</a>
				</li>
			</ul>
			<div className="action-buttons">
				<div className="mode-changer">
					{mode=='day'?
						<div className="moon mode-changer-icon" onClick={modeChanger}>
							<Moon/> {/* if day, show night trigger */}
						</div>
						:
						<div className="sun mode-changer-icon" onClick={modeChanger}>
							<Sun/> {/* if night, show day trigger */}
						</div>
					}
				</div>
				<button className='sales demo-trigger'><span>Request Live Demo</span></button>
			</div>
			<div className="overlay-menu">
				<ul className="mobile-menu">
					<li className="mobile-menu-item" onClick={menuToggle}><a href="#aboutus">About us</a></li>
					<li className="mobile-menu-item" onClick={menuToggle}><a href="#team">Team</a></li>
					<li className="mobile-menu-item" onClick={menuToggle}><a href="#partners">Partners</a></li>
					<li className="mobile-menu-item"><a href="http://mammography.alixir.ai/">AIMS</a></li>
					<li className="mobile-menu-item">
						<button className='sales demo-trigger'><span>Request Live Demo</span></button>
					</li>
				</ul>
			</div>
		</div>
	)
}
export default Header;