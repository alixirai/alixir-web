import Header from '../components/Header'
import Footer from '../components/Footer'

import '../styles/main.scss'
import '../styles/responsive.scss'

const MyApp = ({Component, pageProps}) => (
    <div className='page'>
        <Header/>
        <div className="page-content">
            <Component {...pageProps} />
        </div>
        <Footer />
        <script src="/js/TweenLite.min.js"></script>
        <script src="/js/EasePack.min.js"></script>
        <script src="/js/banner_particle.js"></script>
    </div>
)

export default MyApp