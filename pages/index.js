import Head from 'next/head'
import LottieAnimation from '../components/Lottie'
import RequestModal from '../components/RequestModal'

import Faster from '../animations/faster.json'
import MoreAccurate from '../animations/more_accurate.json'
import PriceDown from '../animations/price_down.json'
import Linkedin from '../components/svg-components/linkedin'

// Prismic

import { getPage } from '../support/prismic'

const Home = (res) => {

	// Get SSR content from Prismic
	const content = res.response

	return (
		<div className="main">

			<Head>
				<title>{content?.meta_title?.[0]?.text}</title>
				<meta name="description" content={content?.meta_description?.[0]?.text}/>
				<link rel="icon" href="/logo.png" />
			</Head>

			<section className="banner">
				<div className="banner-text">
					<h1 className='banner-text-title text'>
						{content?.header_heading?.[0]?.text}
					</h1>
					<p className='banner-text-subtitle text'>
						{content?.header_subheading?.[0]?.text}
					</p>
				</div>
				<div className="banner-mgm">
					<canvas className="banner-mgm-canvas"></canvas>
					<img src="/images/banner_mammo.png" className="mammo-img" alt="breastNN" />
				</div>
			</section>
			
			<section className="company-keypoints" id="aboutus">
				<h1 className="company-keypoints-title heading">Bringing <span>Deep Learning</span> to <span>Digital Mammography</span></h1>
				<div className="company-keypoints-perks">
					<div className="company-keypoints-perks-perk">
						<div className="perk-animation">
							<LottieAnimation lotti={MoreAccurate} />
						</div>
						<div className="perk-description">
							<h3 className='perk-description-title'>{content?.key_features?.[0]?.header?.[0].text}</h3>
							<p className='perk-description-text'>
								{content?.key_features?.[0]?.text?.[1].text}
							</p>
						</div>
					</div>
					<div className="company-keypoints-perks-perk">
						<div className="perk-description">
							<h3 className='perk-description-title'>{content?.key_features?.[1]?.header?.[0].text}</h3>
							<p className='perk-description-text'>
								{content?.key_features?.[1]?.text?.[0].text}
							</p>
						</div>
						<div className="perk-animation">
							<LottieAnimation lotti={Faster} />
						</div>
					</div>
					<div className="company-keypoints-perks-perk">
						<div className="perk-animation">
							<LottieAnimation lotti={PriceDown} speed={2} />
						</div>
						<div className="perk-description">
							<h3 className='perk-description-title'>{content?.key_features?.[2]?.header?.[0].text}</h3>
							<p className='perk-description-text'>
								{content?.key_features?.[2]?.text?.[0].text}
							</p>
						</div>
					</div>
				</div>
			</section>
			<section className="team" id="team">
				<h1 className="team-heading heading">Team <span>Alixir</span></h1>
				<div className="team-row">
					{ content?.team.map( (item) => {
						console.log(item)
						return (
							<figure className="team-member">
							<div className="team-member-dp">
								<img src={item?.image?.url} alt="Joe Logan"/>
							</div>
						<p className="team-member-name">{item?.name?.[0].text}</p>
						<figcaption>
							<span className="person-title">
								<p className="person-title-name">{item?.name?.[0].text}</p>
								<p className="person-title-role">{item?.role?.[0].text}</p>
							</span>
							<p className='person-description'>
								{item?.bio?.[0].text}
								<a href={item?.linkedin?.url} target="_blank" className="connect-linkedin">
									<Linkedin />
								</a>
							</p>
						</figcaption>
					</figure>
						)
					})}

			
				</div>
			</section>
			<section className="partners" id="partners">
				<h1 className="partners-heading heading">Our <span>Partners</span></h1>
				<div className="partners-links">

					{content?.partners?.map( (item) => {
						return (
							<a href={item?.link?.url} target="_blank">
								<img src={item?.logo?.url} alt="Partner Image" />
							</a>
						)
					})}

				</div>
			</section>
			<section className="tryDemo">
				<h1 className="tryDemo-heading heading invert">
					Try <span>AIMS</span> today
					<button className="btn action demo-trigger">
						<span>Request Live Demo</span>
					</button>
				</h1>
			</section>
			<RequestModal/>
		</div>
	)
}

export async function getServerSideProps() {
	const response = await getPage('homepage', false)
	return {
	  props: {response: response},
	}
  }

export default Home;