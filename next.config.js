const withImages = require('next-images')
module.exports = withImages({
    publicRuntimeConfig: {
        MAILCHIMP_URL: process.env.REACT_APP_MAILCHIMP_URL ? process.env.REACT_APP_MAILCHIMP_URL : 'http://localhost:3000/',
    }
})